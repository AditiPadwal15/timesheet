package portal.web.rest;

import portal.Application;
import portal.domain.Tb_task;
import portal.repository.Tb_taskRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_taskResource REST controller.
 *
 * @see Tb_taskResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_taskResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Float DEFAULT_HOURS = 1F;
    private static final Float UPDATED_HOURS = 2F;

    private static final ZonedDateTime DEFAULT_CREATED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_ON_STR = dateTimeFormatter.format(DEFAULT_CREATED_ON);
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final ZonedDateTime DEFAULT_MODIFIED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_MODIFIED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_MODIFIED_ON_STR = dateTimeFormatter.format(DEFAULT_MODIFIED_ON);
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";

    @Inject
    private Tb_taskRepository tb_taskRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_taskMockMvc;

    private Tb_task tb_task;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_taskResource tb_taskResource = new Tb_taskResource();
        ReflectionTestUtils.setField(tb_taskResource, "tb_taskRepository", tb_taskRepository);
        this.restTb_taskMockMvc = MockMvcBuilders.standaloneSetup(tb_taskResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_task = new Tb_task();
        tb_task.setName(DEFAULT_NAME);
        tb_task.setDescription(DEFAULT_DESCRIPTION);
        tb_task.setHours(DEFAULT_HOURS);
        tb_task.setCreated_on(DEFAULT_CREATED_ON);
        tb_task.setCreated_by(DEFAULT_CREATED_BY);
        tb_task.setModified_on(DEFAULT_MODIFIED_ON);
        tb_task.setModified_by(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createTb_task() throws Exception {
        int databaseSizeBeforeCreate = tb_taskRepository.findAll().size();

        // Create the Tb_task

        restTb_taskMockMvc.perform(post("/api/tb_tasks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_task)))
                .andExpect(status().isCreated());

        // Validate the Tb_task in the database
        List<Tb_task> tb_tasks = tb_taskRepository.findAll();
        assertThat(tb_tasks).hasSize(databaseSizeBeforeCreate + 1);
        Tb_task testTb_task = tb_tasks.get(tb_tasks.size() - 1);
        assertThat(testTb_task.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTb_task.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTb_task.getHours()).isEqualTo(DEFAULT_HOURS);
        assertThat(testTb_task.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_task.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_task.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_task.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllTb_tasks() throws Exception {
        // Initialize the database
        tb_taskRepository.saveAndFlush(tb_task);

        // Get all the tb_tasks
        restTb_taskMockMvc.perform(get("/api/tb_tasks?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_task.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].hours").value(hasItem(DEFAULT_HOURS.doubleValue())))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON_STR)))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON_STR)))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getTb_task() throws Exception {
        // Initialize the database
        tb_taskRepository.saveAndFlush(tb_task);

        // Get the tb_task
        restTb_taskMockMvc.perform(get("/api/tb_tasks/{id}", tb_task.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_task.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.hours").value(DEFAULT_HOURS.doubleValue()))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON_STR))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON_STR))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_task() throws Exception {
        // Get the tb_task
        restTb_taskMockMvc.perform(get("/api/tb_tasks/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_task() throws Exception {
        // Initialize the database
        tb_taskRepository.saveAndFlush(tb_task);

		int databaseSizeBeforeUpdate = tb_taskRepository.findAll().size();

        // Update the tb_task
        tb_task.setName(UPDATED_NAME);
        tb_task.setDescription(UPDATED_DESCRIPTION);
        tb_task.setHours(UPDATED_HOURS);
        tb_task.setCreated_on(UPDATED_CREATED_ON);
        tb_task.setCreated_by(UPDATED_CREATED_BY);
        tb_task.setModified_on(UPDATED_MODIFIED_ON);
        tb_task.setModified_by(UPDATED_MODIFIED_BY);

        restTb_taskMockMvc.perform(put("/api/tb_tasks")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_task)))
                .andExpect(status().isOk());

        // Validate the Tb_task in the database
        List<Tb_task> tb_tasks = tb_taskRepository.findAll();
        assertThat(tb_tasks).hasSize(databaseSizeBeforeUpdate);
        Tb_task testTb_task = tb_tasks.get(tb_tasks.size() - 1);
        assertThat(testTb_task.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTb_task.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTb_task.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testTb_task.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_task.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_task.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_task.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void deleteTb_task() throws Exception {
        // Initialize the database
        tb_taskRepository.saveAndFlush(tb_task);

		int databaseSizeBeforeDelete = tb_taskRepository.findAll().size();

        // Get the tb_task
        restTb_taskMockMvc.perform(delete("/api/tb_tasks/{id}", tb_task.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_task> tb_tasks = tb_taskRepository.findAll();
        assertThat(tb_tasks).hasSize(databaseSizeBeforeDelete - 1);
    }
}
