package portal.web.rest;

import portal.Application;
import portal.domain.Tb_timesheet;
import portal.repository.Tb_timesheetRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_timesheetResource REST controller.
 *
 * @see Tb_timesheetResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_timesheetResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final LocalDate DEFAULT_DATEOFTIMESHEET = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATEOFTIMESHEET = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_WORKINGHOURS = 1F;
    private static final Float UPDATED_WORKINGHOURS = 2F;

    private static final Float DEFAULT_BILLABLEHOURS = 1F;
    private static final Float UPDATED_BILLABLEHOURS = 2F;
    private static final String DEFAULT_SHIFT = "AAAAA";
    private static final String UPDATED_SHIFT = "BBBBB";

    private static final LocalDate DEFAULT_CREATED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final LocalDate DEFAULT_APPROVALDATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_APPROVALDATE = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";
    private static final String DEFAULT_REMARK = "AAAAA";
    private static final String UPDATED_REMARK = "BBBBB";

    private static final Integer DEFAULT_WEEKNO = 1;
    private static final Integer UPDATED_WEEKNO = 2;

    private static final LocalDate DEFAULT_MODIFIED_ON = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFIED_ON = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";

    @Inject
    private Tb_timesheetRepository tb_timesheetRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_timesheetMockMvc;

    private Tb_timesheet tb_timesheet;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_timesheetResource tb_timesheetResource = new Tb_timesheetResource();
        ReflectionTestUtils.setField(tb_timesheetResource, "tb_timesheetRepository", tb_timesheetRepository);
        this.restTb_timesheetMockMvc = MockMvcBuilders.standaloneSetup(tb_timesheetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_timesheet = new Tb_timesheet();
        tb_timesheet.setName(DEFAULT_NAME);
        tb_timesheet.setDateoftimesheet(DEFAULT_DATEOFTIMESHEET);
        tb_timesheet.setWorkinghours(DEFAULT_WORKINGHOURS);
        tb_timesheet.setBillablehours(DEFAULT_BILLABLEHOURS);
        tb_timesheet.setShift(DEFAULT_SHIFT);
        tb_timesheet.setCreated_on(DEFAULT_CREATED_ON);
        tb_timesheet.setCreated_by(DEFAULT_CREATED_BY);
        tb_timesheet.setApprovaldate(DEFAULT_APPROVALDATE);
        tb_timesheet.setStatus(DEFAULT_STATUS);
        tb_timesheet.setRemark(DEFAULT_REMARK);
        tb_timesheet.setWeekno(DEFAULT_WEEKNO);
        tb_timesheet.setModified_on(DEFAULT_MODIFIED_ON);
        tb_timesheet.setModified_by(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createTb_timesheet() throws Exception {
        int databaseSizeBeforeCreate = tb_timesheetRepository.findAll().size();

        // Create the Tb_timesheet

        restTb_timesheetMockMvc.perform(post("/api/tb_timesheets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_timesheet)))
                .andExpect(status().isCreated());

        // Validate the Tb_timesheet in the database
        List<Tb_timesheet> tb_timesheets = tb_timesheetRepository.findAll();
        assertThat(tb_timesheets).hasSize(databaseSizeBeforeCreate + 1);
        Tb_timesheet testTb_timesheet = tb_timesheets.get(tb_timesheets.size() - 1);
        assertThat(testTb_timesheet.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTb_timesheet.getDateoftimesheet()).isEqualTo(DEFAULT_DATEOFTIMESHEET);
        assertThat(testTb_timesheet.getWorkinghours()).isEqualTo(DEFAULT_WORKINGHOURS);
        assertThat(testTb_timesheet.getBillablehours()).isEqualTo(DEFAULT_BILLABLEHOURS);
        assertThat(testTb_timesheet.getShift()).isEqualTo(DEFAULT_SHIFT);
        assertThat(testTb_timesheet.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_timesheet.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_timesheet.getApprovaldate()).isEqualTo(DEFAULT_APPROVALDATE);
        assertThat(testTb_timesheet.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTb_timesheet.getRemark()).isEqualTo(DEFAULT_REMARK);
        assertThat(testTb_timesheet.getWeekno()).isEqualTo(DEFAULT_WEEKNO);
        assertThat(testTb_timesheet.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_timesheet.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllTb_timesheets() throws Exception {
        // Initialize the database
        tb_timesheetRepository.saveAndFlush(tb_timesheet);

        // Get all the tb_timesheets
        restTb_timesheetMockMvc.perform(get("/api/tb_timesheets?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_timesheet.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].dateoftimesheet").value(hasItem(DEFAULT_DATEOFTIMESHEET.toString())))
                .andExpect(jsonPath("$.[*].workinghours").value(hasItem(DEFAULT_WORKINGHOURS.doubleValue())))
                .andExpect(jsonPath("$.[*].billablehours").value(hasItem(DEFAULT_BILLABLEHOURS.doubleValue())))
                .andExpect(jsonPath("$.[*].shift").value(hasItem(DEFAULT_SHIFT.toString())))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON.toString())))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].approvaldate").value(hasItem(DEFAULT_APPROVALDATE.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].remark").value(hasItem(DEFAULT_REMARK.toString())))
                .andExpect(jsonPath("$.[*].weekno").value(hasItem(DEFAULT_WEEKNO)))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON.toString())))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getTb_timesheet() throws Exception {
        // Initialize the database
        tb_timesheetRepository.saveAndFlush(tb_timesheet);

        // Get the tb_timesheet
        restTb_timesheetMockMvc.perform(get("/api/tb_timesheets/{id}", tb_timesheet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_timesheet.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.dateoftimesheet").value(DEFAULT_DATEOFTIMESHEET.toString()))
            .andExpect(jsonPath("$.workinghours").value(DEFAULT_WORKINGHOURS.doubleValue()))
            .andExpect(jsonPath("$.billablehours").value(DEFAULT_BILLABLEHOURS.doubleValue()))
            .andExpect(jsonPath("$.shift").value(DEFAULT_SHIFT.toString()))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.approvaldate").value(DEFAULT_APPROVALDATE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.remark").value(DEFAULT_REMARK.toString()))
            .andExpect(jsonPath("$.weekno").value(DEFAULT_WEEKNO))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON.toString()))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_timesheet() throws Exception {
        // Get the tb_timesheet
        restTb_timesheetMockMvc.perform(get("/api/tb_timesheets/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_timesheet() throws Exception {
        // Initialize the database
        tb_timesheetRepository.saveAndFlush(tb_timesheet);

		int databaseSizeBeforeUpdate = tb_timesheetRepository.findAll().size();

        // Update the tb_timesheet
        tb_timesheet.setName(UPDATED_NAME);
        tb_timesheet.setDateoftimesheet(UPDATED_DATEOFTIMESHEET);
        tb_timesheet.setWorkinghours(UPDATED_WORKINGHOURS);
        tb_timesheet.setBillablehours(UPDATED_BILLABLEHOURS);
        tb_timesheet.setShift(UPDATED_SHIFT);
        tb_timesheet.setCreated_on(UPDATED_CREATED_ON);
        tb_timesheet.setCreated_by(UPDATED_CREATED_BY);
        tb_timesheet.setApprovaldate(UPDATED_APPROVALDATE);
        tb_timesheet.setStatus(UPDATED_STATUS);
        tb_timesheet.setRemark(UPDATED_REMARK);
        tb_timesheet.setWeekno(UPDATED_WEEKNO);
        tb_timesheet.setModified_on(UPDATED_MODIFIED_ON);
        tb_timesheet.setModified_by(UPDATED_MODIFIED_BY);

        restTb_timesheetMockMvc.perform(put("/api/tb_timesheets")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_timesheet)))
                .andExpect(status().isOk());

        // Validate the Tb_timesheet in the database
        List<Tb_timesheet> tb_timesheets = tb_timesheetRepository.findAll();
        assertThat(tb_timesheets).hasSize(databaseSizeBeforeUpdate);
        Tb_timesheet testTb_timesheet = tb_timesheets.get(tb_timesheets.size() - 1);
        assertThat(testTb_timesheet.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTb_timesheet.getDateoftimesheet()).isEqualTo(UPDATED_DATEOFTIMESHEET);
        assertThat(testTb_timesheet.getWorkinghours()).isEqualTo(UPDATED_WORKINGHOURS);
        assertThat(testTb_timesheet.getBillablehours()).isEqualTo(UPDATED_BILLABLEHOURS);
        assertThat(testTb_timesheet.getShift()).isEqualTo(UPDATED_SHIFT);
        assertThat(testTb_timesheet.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_timesheet.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_timesheet.getApprovaldate()).isEqualTo(UPDATED_APPROVALDATE);
        assertThat(testTb_timesheet.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTb_timesheet.getRemark()).isEqualTo(UPDATED_REMARK);
        assertThat(testTb_timesheet.getWeekno()).isEqualTo(UPDATED_WEEKNO);
        assertThat(testTb_timesheet.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_timesheet.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void deleteTb_timesheet() throws Exception {
        // Initialize the database
        tb_timesheetRepository.saveAndFlush(tb_timesheet);

		int databaseSizeBeforeDelete = tb_timesheetRepository.findAll().size();

        // Get the tb_timesheet
        restTb_timesheetMockMvc.perform(delete("/api/tb_timesheets/{id}", tb_timesheet.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_timesheet> tb_timesheets = tb_timesheetRepository.findAll();
        assertThat(tb_timesheets).hasSize(databaseSizeBeforeDelete - 1);
    }
}
