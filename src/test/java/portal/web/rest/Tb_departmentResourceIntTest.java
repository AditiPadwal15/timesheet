package portal.web.rest;

import portal.Application;
import portal.domain.Tb_department;
import portal.repository.Tb_departmentRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_departmentResource REST controller.
 *
 * @see Tb_departmentResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_departmentResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_ON_STR = dateTimeFormatter.format(DEFAULT_CREATED_ON);
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final ZonedDateTime DEFAULT_MODIFIED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_MODIFIED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_MODIFIED_ON_STR = dateTimeFormatter.format(DEFAULT_MODIFIED_ON);
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";

    @Inject
    private Tb_departmentRepository tb_departmentRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_departmentMockMvc;

    private Tb_department tb_department;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_departmentResource tb_departmentResource = new Tb_departmentResource();
        ReflectionTestUtils.setField(tb_departmentResource, "tb_departmentRepository", tb_departmentRepository);
        this.restTb_departmentMockMvc = MockMvcBuilders.standaloneSetup(tb_departmentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_department = new Tb_department();
        tb_department.setName(DEFAULT_NAME);
        tb_department.setDescription(DEFAULT_DESCRIPTION);
        tb_department.setCreated_on(DEFAULT_CREATED_ON);
        tb_department.setCreated_by(DEFAULT_CREATED_BY);
        tb_department.setModified_on(DEFAULT_MODIFIED_ON);
        tb_department.setModified_by(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createTb_department() throws Exception {
        int databaseSizeBeforeCreate = tb_departmentRepository.findAll().size();

        // Create the Tb_department

        restTb_departmentMockMvc.perform(post("/api/tb_departments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_department)))
                .andExpect(status().isCreated());

        // Validate the Tb_department in the database
        List<Tb_department> tb_departments = tb_departmentRepository.findAll();
        assertThat(tb_departments).hasSize(databaseSizeBeforeCreate + 1);
        Tb_department testTb_department = tb_departments.get(tb_departments.size() - 1);
        assertThat(testTb_department.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTb_department.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTb_department.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_department.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_department.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_department.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllTb_departments() throws Exception {
        // Initialize the database
        tb_departmentRepository.saveAndFlush(tb_department);

        // Get all the tb_departments
        restTb_departmentMockMvc.perform(get("/api/tb_departments?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_department.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON_STR)))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON_STR)))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getTb_department() throws Exception {
        // Initialize the database
        tb_departmentRepository.saveAndFlush(tb_department);

        // Get the tb_department
        restTb_departmentMockMvc.perform(get("/api/tb_departments/{id}", tb_department.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_department.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON_STR))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON_STR))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_department() throws Exception {
        // Get the tb_department
        restTb_departmentMockMvc.perform(get("/api/tb_departments/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_department() throws Exception {
        // Initialize the database
        tb_departmentRepository.saveAndFlush(tb_department);

		int databaseSizeBeforeUpdate = tb_departmentRepository.findAll().size();

        // Update the tb_department
        tb_department.setName(UPDATED_NAME);
        tb_department.setDescription(UPDATED_DESCRIPTION);
        tb_department.setCreated_on(UPDATED_CREATED_ON);
        tb_department.setCreated_by(UPDATED_CREATED_BY);
        tb_department.setModified_on(UPDATED_MODIFIED_ON);
        tb_department.setModified_by(UPDATED_MODIFIED_BY);

        restTb_departmentMockMvc.perform(put("/api/tb_departments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_department)))
                .andExpect(status().isOk());

        // Validate the Tb_department in the database
        List<Tb_department> tb_departments = tb_departmentRepository.findAll();
        assertThat(tb_departments).hasSize(databaseSizeBeforeUpdate);
        Tb_department testTb_department = tb_departments.get(tb_departments.size() - 1);
        assertThat(testTb_department.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTb_department.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTb_department.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_department.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_department.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_department.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void deleteTb_department() throws Exception {
        // Initialize the database
        tb_departmentRepository.saveAndFlush(tb_department);

		int databaseSizeBeforeDelete = tb_departmentRepository.findAll().size();

        // Get the tb_department
        restTb_departmentMockMvc.perform(delete("/api/tb_departments/{id}", tb_department.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_department> tb_departments = tb_departmentRepository.findAll();
        assertThat(tb_departments).hasSize(databaseSizeBeforeDelete - 1);
    }
}
