package portal.web.rest;

import portal.Application;
import portal.domain.Tb_client;
import portal.repository.Tb_clientRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_clientResource REST controller.
 *
 * @see Tb_clientResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_clientResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_ON_STR = dateTimeFormatter.format(DEFAULT_CREATED_ON);
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final ZonedDateTime DEFAULT_MODIFIED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_MODIFIED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_MODIFIED_ON_STR = dateTimeFormatter.format(DEFAULT_MODIFIED_ON);
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    @Inject
    private Tb_clientRepository tb_clientRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_clientMockMvc;

    private Tb_client tb_client;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_clientResource tb_clientResource = new Tb_clientResource();
        ReflectionTestUtils.setField(tb_clientResource, "tb_clientRepository", tb_clientRepository);
        this.restTb_clientMockMvc = MockMvcBuilders.standaloneSetup(tb_clientResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_client = new Tb_client();
        tb_client.setName(DEFAULT_NAME);
        tb_client.setDescription(DEFAULT_DESCRIPTION);
        tb_client.setCreated_on(DEFAULT_CREATED_ON);
        tb_client.setCreated_by(DEFAULT_CREATED_BY);
        tb_client.setModified_on(DEFAULT_MODIFIED_ON);
        tb_client.setModified_by(DEFAULT_MODIFIED_BY);
        tb_client.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTb_client() throws Exception {
        int databaseSizeBeforeCreate = tb_clientRepository.findAll().size();

        // Create the Tb_client

        restTb_clientMockMvc.perform(post("/api/tb_clients")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_client)))
                .andExpect(status().isCreated());

        // Validate the Tb_client in the database
        List<Tb_client> tb_clients = tb_clientRepository.findAll();
        assertThat(tb_clients).hasSize(databaseSizeBeforeCreate + 1);
        Tb_client testTb_client = tb_clients.get(tb_clients.size() - 1);
        assertThat(testTb_client.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTb_client.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTb_client.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_client.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_client.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_client.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testTb_client.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllTb_clients() throws Exception {
        // Initialize the database
        tb_clientRepository.saveAndFlush(tb_client);

        // Get all the tb_clients
        restTb_clientMockMvc.perform(get("/api/tb_clients?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_client.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON_STR)))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON_STR)))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getTb_client() throws Exception {
        // Initialize the database
        tb_clientRepository.saveAndFlush(tb_client);

        // Get the tb_client
        restTb_clientMockMvc.perform(get("/api/tb_clients/{id}", tb_client.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_client.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON_STR))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON_STR))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_client() throws Exception {
        // Get the tb_client
        restTb_clientMockMvc.perform(get("/api/tb_clients/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_client() throws Exception {
        // Initialize the database
        tb_clientRepository.saveAndFlush(tb_client);

		int databaseSizeBeforeUpdate = tb_clientRepository.findAll().size();

        // Update the tb_client
        tb_client.setName(UPDATED_NAME);
        tb_client.setDescription(UPDATED_DESCRIPTION);
        tb_client.setCreated_on(UPDATED_CREATED_ON);
        tb_client.setCreated_by(UPDATED_CREATED_BY);
        tb_client.setModified_on(UPDATED_MODIFIED_ON);
        tb_client.setModified_by(UPDATED_MODIFIED_BY);
        tb_client.setStatus(UPDATED_STATUS);

        restTb_clientMockMvc.perform(put("/api/tb_clients")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_client)))
                .andExpect(status().isOk());

        // Validate the Tb_client in the database
        List<Tb_client> tb_clients = tb_clientRepository.findAll();
        assertThat(tb_clients).hasSize(databaseSizeBeforeUpdate);
        Tb_client testTb_client = tb_clients.get(tb_clients.size() - 1);
        assertThat(testTb_client.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTb_client.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTb_client.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_client.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_client.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_client.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testTb_client.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteTb_client() throws Exception {
        // Initialize the database
        tb_clientRepository.saveAndFlush(tb_client);

		int databaseSizeBeforeDelete = tb_clientRepository.findAll().size();

        // Get the tb_client
        restTb_clientMockMvc.perform(delete("/api/tb_clients/{id}", tb_client.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_client> tb_clients = tb_clientRepository.findAll();
        assertThat(tb_clients).hasSize(databaseSizeBeforeDelete - 1);
    }
}
