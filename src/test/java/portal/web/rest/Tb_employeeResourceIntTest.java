package portal.web.rest;

import portal.Application;
import portal.domain.Tb_employee;
import portal.repository.Tb_employeeRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_employeeResource REST controller.
 *
 * @see Tb_employeeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_employeeResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_DESIGNATION = "AAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBB";

    private static final Boolean DEFAULT_ONSITE = false;
    private static final Boolean UPDATED_ONSITE = true;
    private static final String DEFAULT_COMPANYNAME = "AAAAA";
    private static final String UPDATED_COMPANYNAME = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    private static final Integer DEFAULT_SUPERVISOR_ID = 1;
    private static final Integer UPDATED_SUPERVISOR_ID = 2;

    private static final ZonedDateTime DEFAULT_CREATED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_ON_STR = dateTimeFormatter.format(DEFAULT_CREATED_ON);
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final ZonedDateTime DEFAULT_MODIFIED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_MODIFIED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_MODIFIED_ON_STR = dateTimeFormatter.format(DEFAULT_MODIFIED_ON);
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";

    @Inject
    private Tb_employeeRepository tb_employeeRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_employeeMockMvc;

    private Tb_employee tb_employee;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_employeeResource tb_employeeResource = new Tb_employeeResource();
        ReflectionTestUtils.setField(tb_employeeResource, "tb_employeeRepository", tb_employeeRepository);
        this.restTb_employeeMockMvc = MockMvcBuilders.standaloneSetup(tb_employeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_employee = new Tb_employee();
        tb_employee.setDesignation(DEFAULT_DESIGNATION);
        tb_employee.setOnsite(DEFAULT_ONSITE);
        tb_employee.setCompanyname(DEFAULT_COMPANYNAME);
        tb_employee.setStatus(DEFAULT_STATUS);
        tb_employee.setSupervisor_id(DEFAULT_SUPERVISOR_ID);
        tb_employee.setCreated_on(DEFAULT_CREATED_ON);
        tb_employee.setCreated_by(DEFAULT_CREATED_BY);
        tb_employee.setModified_on(DEFAULT_MODIFIED_ON);
        tb_employee.setModified_by(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createTb_employee() throws Exception {
        int databaseSizeBeforeCreate = tb_employeeRepository.findAll().size();

        // Create the Tb_employee

        restTb_employeeMockMvc.perform(post("/api/tb_employees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_employee)))
                .andExpect(status().isCreated());

        // Validate the Tb_employee in the database
        List<Tb_employee> tb_employees = tb_employeeRepository.findAll();
        assertThat(tb_employees).hasSize(databaseSizeBeforeCreate + 1);
        Tb_employee testTb_employee = tb_employees.get(tb_employees.size() - 1);
        assertThat(testTb_employee.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testTb_employee.getOnsite()).isEqualTo(DEFAULT_ONSITE);
        assertThat(testTb_employee.getCompanyname()).isEqualTo(DEFAULT_COMPANYNAME);
        assertThat(testTb_employee.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTb_employee.getSupervisor_id()).isEqualTo(DEFAULT_SUPERVISOR_ID);
        assertThat(testTb_employee.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_employee.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_employee.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_employee.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllTb_employees() throws Exception {
        // Initialize the database
        tb_employeeRepository.saveAndFlush(tb_employee);

        // Get all the tb_employees
        restTb_employeeMockMvc.perform(get("/api/tb_employees?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_employee.getId().intValue())))
                .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION.toString())))
                .andExpect(jsonPath("$.[*].onsite").value(hasItem(DEFAULT_ONSITE.booleanValue())))
                .andExpect(jsonPath("$.[*].companyname").value(hasItem(DEFAULT_COMPANYNAME.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].supervisor_id").value(hasItem(DEFAULT_SUPERVISOR_ID)))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON_STR)))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON_STR)))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getTb_employee() throws Exception {
        // Initialize the database
        tb_employeeRepository.saveAndFlush(tb_employee);

        // Get the tb_employee
        restTb_employeeMockMvc.perform(get("/api/tb_employees/{id}", tb_employee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_employee.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION.toString()))
            .andExpect(jsonPath("$.onsite").value(DEFAULT_ONSITE.booleanValue()))
            .andExpect(jsonPath("$.companyname").value(DEFAULT_COMPANYNAME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.supervisor_id").value(DEFAULT_SUPERVISOR_ID))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON_STR))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON_STR))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_employee() throws Exception {
        // Get the tb_employee
        restTb_employeeMockMvc.perform(get("/api/tb_employees/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_employee() throws Exception {
        // Initialize the database
        tb_employeeRepository.saveAndFlush(tb_employee);

		int databaseSizeBeforeUpdate = tb_employeeRepository.findAll().size();

        // Update the tb_employee
        tb_employee.setDesignation(UPDATED_DESIGNATION);
        tb_employee.setOnsite(UPDATED_ONSITE);
        tb_employee.setCompanyname(UPDATED_COMPANYNAME);
        tb_employee.setStatus(UPDATED_STATUS);
        tb_employee.setSupervisor_id(UPDATED_SUPERVISOR_ID);
        tb_employee.setCreated_on(UPDATED_CREATED_ON);
        tb_employee.setCreated_by(UPDATED_CREATED_BY);
        tb_employee.setModified_on(UPDATED_MODIFIED_ON);
        tb_employee.setModified_by(UPDATED_MODIFIED_BY);

        restTb_employeeMockMvc.perform(put("/api/tb_employees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_employee)))
                .andExpect(status().isOk());

        // Validate the Tb_employee in the database
        List<Tb_employee> tb_employees = tb_employeeRepository.findAll();
        assertThat(tb_employees).hasSize(databaseSizeBeforeUpdate);
        Tb_employee testTb_employee = tb_employees.get(tb_employees.size() - 1);
        assertThat(testTb_employee.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testTb_employee.getOnsite()).isEqualTo(UPDATED_ONSITE);
        assertThat(testTb_employee.getCompanyname()).isEqualTo(UPDATED_COMPANYNAME);
        assertThat(testTb_employee.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTb_employee.getSupervisor_id()).isEqualTo(UPDATED_SUPERVISOR_ID);
        assertThat(testTb_employee.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_employee.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_employee.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_employee.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void deleteTb_employee() throws Exception {
        // Initialize the database
        tb_employeeRepository.saveAndFlush(tb_employee);

		int databaseSizeBeforeDelete = tb_employeeRepository.findAll().size();

        // Get the tb_employee
        restTb_employeeMockMvc.perform(delete("/api/tb_employees/{id}", tb_employee.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_employee> tb_employees = tb_employeeRepository.findAll();
        assertThat(tb_employees).hasSize(databaseSizeBeforeDelete - 1);
    }
}
