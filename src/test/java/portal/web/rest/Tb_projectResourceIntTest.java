package portal.web.rest;

import portal.Application;
import portal.domain.Tb_project;
import portal.repository.Tb_projectRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Tb_projectResource REST controller.
 *
 * @see Tb_projectResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Tb_projectResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME.withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_CREATED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_ON_STR = dateTimeFormatter.format(DEFAULT_CREATED_ON);
    private static final String DEFAULT_CREATED_BY = "AAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBB";

    private static final ZonedDateTime DEFAULT_MODIFIED_ON = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_MODIFIED_ON = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_MODIFIED_ON_STR = dateTimeFormatter.format(DEFAULT_MODIFIED_ON);
    private static final String DEFAULT_MODIFIED_BY = "AAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBB";
    private static final String DEFAULT_STATUS = "AAAAA";
    private static final String UPDATED_STATUS = "BBBBB";

    @Inject
    private Tb_projectRepository tb_projectRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTb_projectMockMvc;

    private Tb_project tb_project;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Tb_projectResource tb_projectResource = new Tb_projectResource();
        ReflectionTestUtils.setField(tb_projectResource, "tb_projectRepository", tb_projectRepository);
        this.restTb_projectMockMvc = MockMvcBuilders.standaloneSetup(tb_projectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tb_project = new Tb_project();
        tb_project.setName(DEFAULT_NAME);
        tb_project.setDescription(DEFAULT_DESCRIPTION);
        tb_project.setStart_date(DEFAULT_START_DATE);
        tb_project.setEnd_date(DEFAULT_END_DATE);
        tb_project.setCreated_on(DEFAULT_CREATED_ON);
        tb_project.setCreated_by(DEFAULT_CREATED_BY);
        tb_project.setModified_on(DEFAULT_MODIFIED_ON);
        tb_project.setModified_by(DEFAULT_MODIFIED_BY);
        tb_project.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createTb_project() throws Exception {
        int databaseSizeBeforeCreate = tb_projectRepository.findAll().size();

        // Create the Tb_project

        restTb_projectMockMvc.perform(post("/api/tb_projects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_project)))
                .andExpect(status().isCreated());

        // Validate the Tb_project in the database
        List<Tb_project> tb_projects = tb_projectRepository.findAll();
        assertThat(tb_projects).hasSize(databaseSizeBeforeCreate + 1);
        Tb_project testTb_project = tb_projects.get(tb_projects.size() - 1);
        assertThat(testTb_project.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTb_project.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTb_project.getStart_date()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testTb_project.getEnd_date()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testTb_project.getCreated_on()).isEqualTo(DEFAULT_CREATED_ON);
        assertThat(testTb_project.getCreated_by()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTb_project.getModified_on()).isEqualTo(DEFAULT_MODIFIED_ON);
        assertThat(testTb_project.getModified_by()).isEqualTo(DEFAULT_MODIFIED_BY);
        assertThat(testTb_project.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllTb_projects() throws Exception {
        // Initialize the database
        tb_projectRepository.saveAndFlush(tb_project);

        // Get all the tb_projects
        restTb_projectMockMvc.perform(get("/api/tb_projects?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tb_project.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].start_date").value(hasItem(DEFAULT_START_DATE.toString())))
                .andExpect(jsonPath("$.[*].end_date").value(hasItem(DEFAULT_END_DATE.toString())))
                .andExpect(jsonPath("$.[*].created_on").value(hasItem(DEFAULT_CREATED_ON_STR)))
                .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
                .andExpect(jsonPath("$.[*].modified_on").value(hasItem(DEFAULT_MODIFIED_ON_STR)))
                .andExpect(jsonPath("$.[*].modified_by").value(hasItem(DEFAULT_MODIFIED_BY.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getTb_project() throws Exception {
        // Initialize the database
        tb_projectRepository.saveAndFlush(tb_project);

        // Get the tb_project
        restTb_projectMockMvc.perform(get("/api/tb_projects/{id}", tb_project.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tb_project.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.start_date").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.end_date").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.created_on").value(DEFAULT_CREATED_ON_STR))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modified_on").value(DEFAULT_MODIFIED_ON_STR))
            .andExpect(jsonPath("$.modified_by").value(DEFAULT_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTb_project() throws Exception {
        // Get the tb_project
        restTb_projectMockMvc.perform(get("/api/tb_projects/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTb_project() throws Exception {
        // Initialize the database
        tb_projectRepository.saveAndFlush(tb_project);

		int databaseSizeBeforeUpdate = tb_projectRepository.findAll().size();

        // Update the tb_project
        tb_project.setName(UPDATED_NAME);
        tb_project.setDescription(UPDATED_DESCRIPTION);
        tb_project.setStart_date(UPDATED_START_DATE);
        tb_project.setEnd_date(UPDATED_END_DATE);
        tb_project.setCreated_on(UPDATED_CREATED_ON);
        tb_project.setCreated_by(UPDATED_CREATED_BY);
        tb_project.setModified_on(UPDATED_MODIFIED_ON);
        tb_project.setModified_by(UPDATED_MODIFIED_BY);
        tb_project.setStatus(UPDATED_STATUS);

        restTb_projectMockMvc.perform(put("/api/tb_projects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tb_project)))
                .andExpect(status().isOk());

        // Validate the Tb_project in the database
        List<Tb_project> tb_projects = tb_projectRepository.findAll();
        assertThat(tb_projects).hasSize(databaseSizeBeforeUpdate);
        Tb_project testTb_project = tb_projects.get(tb_projects.size() - 1);
        assertThat(testTb_project.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTb_project.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTb_project.getStart_date()).isEqualTo(UPDATED_START_DATE);
        assertThat(testTb_project.getEnd_date()).isEqualTo(UPDATED_END_DATE);
        assertThat(testTb_project.getCreated_on()).isEqualTo(UPDATED_CREATED_ON);
        assertThat(testTb_project.getCreated_by()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTb_project.getModified_on()).isEqualTo(UPDATED_MODIFIED_ON);
        assertThat(testTb_project.getModified_by()).isEqualTo(UPDATED_MODIFIED_BY);
        assertThat(testTb_project.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteTb_project() throws Exception {
        // Initialize the database
        tb_projectRepository.saveAndFlush(tb_project);

		int databaseSizeBeforeDelete = tb_projectRepository.findAll().size();

        // Get the tb_project
        restTb_projectMockMvc.perform(delete("/api/tb_projects/{id}", tb_project.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tb_project> tb_projects = tb_projectRepository.findAll();
        assertThat(tb_projects).hasSize(databaseSizeBeforeDelete - 1);
    }
}
