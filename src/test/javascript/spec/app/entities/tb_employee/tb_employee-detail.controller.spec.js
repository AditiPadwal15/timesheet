'use strict';

describe('Controller Tests', function() {

    describe('Tb_employee Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_employee, MockUser, MockTb_project, MockTb_task, MockTb_client, MockTb_department;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_employee = jasmine.createSpy('MockTb_employee');
            MockUser = jasmine.createSpy('MockUser');
            MockTb_project = jasmine.createSpy('MockTb_project');
            MockTb_task = jasmine.createSpy('MockTb_task');
            MockTb_client = jasmine.createSpy('MockTb_client');
            MockTb_department = jasmine.createSpy('MockTb_department');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_employee': MockTb_employee,
                'User': MockUser,
                'Tb_project': MockTb_project,
                'Tb_task': MockTb_task,
                'Tb_client': MockTb_client,
                'Tb_department': MockTb_department
            };
            createController = function() {
                $injector.get('$controller')("Tb_employeeDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_employeeUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
