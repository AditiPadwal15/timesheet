'use strict';

describe('Controller Tests', function() {

    describe('Tb_project Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_project, MockTb_employee, MockTb_client;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_project = jasmine.createSpy('MockTb_project');
            MockTb_employee = jasmine.createSpy('MockTb_employee');
            MockTb_client = jasmine.createSpy('MockTb_client');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_project': MockTb_project,
                'Tb_employee': MockTb_employee,
                'Tb_client': MockTb_client
            };
            createController = function() {
                $injector.get('$controller')("Tb_projectDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_projectUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
