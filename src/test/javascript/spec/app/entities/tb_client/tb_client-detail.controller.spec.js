'use strict';

describe('Controller Tests', function() {

    describe('Tb_client Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_client;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_client = jasmine.createSpy('MockTb_client');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_client': MockTb_client
            };
            createController = function() {
                $injector.get('$controller')("Tb_clientDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_clientUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
