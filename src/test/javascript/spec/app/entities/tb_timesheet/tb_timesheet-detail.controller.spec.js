'use strict';

describe('Controller Tests', function() {

    describe('Tb_timesheet Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_timesheet, MockTb_employee;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_timesheet = jasmine.createSpy('MockTb_timesheet');
            MockTb_employee = jasmine.createSpy('MockTb_employee');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_timesheet': MockTb_timesheet,
                'Tb_employee': MockTb_employee
            };
            createController = function() {
                $injector.get('$controller')("Tb_timesheetDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_timesheetUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
