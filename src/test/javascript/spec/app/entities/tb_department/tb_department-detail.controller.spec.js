'use strict';

describe('Controller Tests', function() {

    describe('Tb_department Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_department;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_department = jasmine.createSpy('MockTb_department');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_department': MockTb_department
            };
            createController = function() {
                $injector.get('$controller')("Tb_departmentDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_departmentUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
