'use strict';

describe('Controller Tests', function() {

    describe('Tb_task Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTb_task, MockTb_employee, MockTb_project;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTb_task = jasmine.createSpy('MockTb_task');
            MockTb_employee = jasmine.createSpy('MockTb_employee');
            MockTb_project = jasmine.createSpy('MockTb_project');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tb_task': MockTb_task,
                'Tb_employee': MockTb_employee,
                'Tb_project': MockTb_project
            };
            createController = function() {
                $injector.get('$controller')("Tb_taskDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'timesheetApp:tb_taskUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
