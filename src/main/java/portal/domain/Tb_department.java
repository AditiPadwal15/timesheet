package portal.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Tb_department.
 */
@Entity
@Table(name = "tb_department")
public class Tb_department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "created_on")
    private ZonedDateTime created_on;
    
    @Column(name = "created_by")
    private String created_by;
    
    @Column(name = "modified_on")
    private ZonedDateTime modified_on;
    
    @Column(name = "modified_by")
    private String modified_by;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreated_on() {
        return created_on;
    }
    
    public void setCreated_on(ZonedDateTime created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }
    
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public ZonedDateTime getModified_on() {
        return modified_on;
    }
    
    public void setModified_on(ZonedDateTime modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }
    
    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tb_department tb_department = (Tb_department) o;
        if(tb_department.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tb_department.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tb_department{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", created_on='" + created_on + "'" +
            ", created_by='" + created_by + "'" +
            ", modified_on='" + modified_on + "'" +
            ", modified_by='" + modified_by + "'" +
            '}';
    }
}
