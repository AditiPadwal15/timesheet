package portal.domain;

import java.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tb_timesheet.
 */
@Entity
@Table(name = "tb_timesheet")
public class Tb_timesheet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;
    
    @Column(name = "dateoftimesheet")
    private LocalDate dateoftimesheet;
    
    @Column(name = "workinghours")
    private Float workinghours;
    
    @Column(name = "billablehours")
    private Float billablehours;
    
    @Column(name = "shift")
    private String shift;
    
    @Column(name = "created_on")
    private LocalDate created_on;
    
    @Column(name = "created_by")
    private String created_by;
    
    @Column(name = "approvaldate")
    private LocalDate approvaldate;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "remark")
    private String remark;
    
    @Column(name = "weekno")
    private Integer weekno;
    
    @Column(name = "modified_on")
    private LocalDate modified_on;
    
    @Column(name = "modified_by")
    private String modified_by;
    
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Tb_employee employee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDateoftimesheet() {
        return dateoftimesheet;
    }
    
    public void setDateoftimesheet(LocalDate dateoftimesheet) {
        this.dateoftimesheet = dateoftimesheet;
    }

    public Float getWorkinghours() {
        return workinghours;
    }
    
    public void setWorkinghours(Float workinghours) {
        this.workinghours = workinghours;
    }

    public Float getBillablehours() {
        return billablehours;
    }
    
    public void setBillablehours(Float billablehours) {
        this.billablehours = billablehours;
    }

    public String getShift() {
        return shift;
    }
    
    public void setShift(String shift) {
        this.shift = shift;
    }

    public LocalDate getCreated_on() {
        return created_on;
    }
    
    public void setCreated_on(LocalDate created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }
    
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public LocalDate getApprovaldate() {
        return approvaldate;
    }
    
    public void setApprovaldate(LocalDate approvaldate) {
        this.approvaldate = approvaldate;
    }

    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getWeekno() {
        return weekno;
    }
    
    public void setWeekno(Integer weekno) {
        this.weekno = weekno;
    }

    public LocalDate getModified_on() {
        return modified_on;
    }
    
    public void setModified_on(LocalDate modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }
    
    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public Tb_employee getEmployee() {
        return employee;
    }

    public void setEmployee(Tb_employee tb_employee) {
        this.employee = tb_employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tb_timesheet tb_timesheet = (Tb_timesheet) o;
        if(tb_timesheet.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tb_timesheet.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tb_timesheet{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", dateoftimesheet='" + dateoftimesheet + "'" +
            ", workinghours='" + workinghours + "'" +
            ", billablehours='" + billablehours + "'" +
            ", shift='" + shift + "'" +
            ", created_on='" + created_on + "'" +
            ", created_by='" + created_by + "'" +
            ", approvaldate='" + approvaldate + "'" +
            ", status='" + status + "'" +
            ", remark='" + remark + "'" +
            ", weekno='" + weekno + "'" +
            ", modified_on='" + modified_on + "'" +
            ", modified_by='" + modified_by + "'" +
            '}';
    }
}
