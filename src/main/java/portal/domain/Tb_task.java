package portal.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tb_task.
 */
@Entity
@Table(name = "tb_task")
public class Tb_task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "hours")
    private Float hours;
    
    @Column(name = "created_on")
    private ZonedDateTime created_on;
    
    @Column(name = "created_by")
    private String created_by;
    
    @Column(name = "modified_on")
    private ZonedDateTime modified_on;
    
    @Column(name = "modified_by")
    private String modified_by;
    
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Tb_employee employee;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Tb_project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public Float getHours() {
        return hours;
    }
    
    public void setHours(Float hours) {
        this.hours = hours;
    }

    public ZonedDateTime getCreated_on() {
        return created_on;
    }
    
    public void setCreated_on(ZonedDateTime created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }
    
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public ZonedDateTime getModified_on() {
        return modified_on;
    }
    
    public void setModified_on(ZonedDateTime modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }
    
    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public Tb_employee getEmployee() {
        return employee;
    }

    public void setEmployee(Tb_employee tb_employee) {
        this.employee = tb_employee;
    }

    public Tb_project getProject() {
        return project;
    }

    public void setProject(Tb_project tb_project) {
        this.project = tb_project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tb_task tb_task = (Tb_task) o;
        if(tb_task.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tb_task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tb_task{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", hours='" + hours + "'" +
            ", created_on='" + created_on + "'" +
            ", created_by='" + created_by + "'" +
            ", modified_on='" + modified_on + "'" +
            ", modified_by='" + modified_by + "'" +
            '}';
    }
}
