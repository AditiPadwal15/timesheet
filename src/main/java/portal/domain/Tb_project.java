package portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tb_project.
 */
@Entity
@Table(name = "tb_project")
public class Tb_project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "start_date")
    private LocalDate start_date;
    
    @Column(name = "end_date")
    private LocalDate end_date;
    
    @Column(name = "created_on")
    private ZonedDateTime created_on;
    
    @Column(name = "created_by")
    private String created_by;
    
    @Column(name = "modified_on")
    private ZonedDateTime modified_on;
    
    @Column(name = "modified_by")
    private String modified_by;
    
    @Column(name = "status")
    private String status;
    
    @ManyToMany(mappedBy = "projects")
    @JsonIgnore
    private Set<Tb_employee> employees = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Tb_client client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStart_date() {
        return start_date;
    }
    
    public void setStart_date(LocalDate start_date) {
        this.start_date = start_date;
    }

    public LocalDate getEnd_date() {
        return end_date;
    }
    
    public void setEnd_date(LocalDate end_date) {
        this.end_date = end_date;
    }

    public ZonedDateTime getCreated_on() {
        return created_on;
    }
    
    public void setCreated_on(ZonedDateTime created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }
    
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public ZonedDateTime getModified_on() {
        return modified_on;
    }
    
    public void setModified_on(ZonedDateTime modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }
    
    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Tb_employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Tb_employee> tb_employees) {
        this.employees = tb_employees;
    }

    public Tb_client getClient() {
        return client;
    }

    public void setClient(Tb_client tb_client) {
        this.client = tb_client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tb_project tb_project = (Tb_project) o;
        if(tb_project.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tb_project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tb_project{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", start_date='" + start_date + "'" +
            ", end_date='" + end_date + "'" +
            ", created_on='" + created_on + "'" +
            ", created_by='" + created_by + "'" +
            ", modified_on='" + modified_on + "'" +
            ", modified_by='" + modified_by + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
