package portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZonedDateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tb_employee.
 */
@Entity
@Table(name = "tb_employee")
public class Tb_employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "designation")
    private String designation;
    
    @Column(name = "onsite")
    private Boolean onsite;
    
    @Column(name = "companyname")
    private String companyname;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "supervisor_id")
    private Integer supervisor_id;
    
    @Column(name = "created_on")
    private ZonedDateTime created_on;
    
    @Column(name = "created_by")
    private String created_by;
    
    @Column(name = "modified_on")
    private ZonedDateTime modified_on;
    
    @Column(name = "modified_by")
    private String modified_by;
    
    @OneToOne
    private User user;

    @ManyToMany
    @JoinTable(name = "tb_employee_project",
               joinColumns = @JoinColumn(name="tb_employees_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="projects_id", referencedColumnName="ID"))
    private Set<Tb_project> projects = new HashSet<>();

    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private Set<Tb_task> tasks = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Tb_client client;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Tb_department department;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }
    
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Boolean getOnsite() {
        return onsite;
    }
    
    public void setOnsite(Boolean onsite) {
        this.onsite = onsite;
    }

    public String getCompanyname() {
        return companyname;
    }
    
    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSupervisor_id() {
        return supervisor_id;
    }
    
    public void setSupervisor_id(Integer supervisor_id) {
        this.supervisor_id = supervisor_id;
    }

    public ZonedDateTime getCreated_on() {
        return created_on;
    }
    
    public void setCreated_on(ZonedDateTime created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }
    
    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public ZonedDateTime getModified_on() {
        return modified_on;
    }
    
    public void setModified_on(ZonedDateTime modified_on) {
        this.modified_on = modified_on;
    }

    public String getModified_by() {
        return modified_by;
    }
    
    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Tb_project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Tb_project> tb_projects) {
        this.projects = tb_projects;
    }

    public Set<Tb_task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Tb_task> tb_tasks) {
        this.tasks = tb_tasks;
    }

    public Tb_client getClient() {
        return client;
    }

    public void setClient(Tb_client tb_client) {
        this.client = tb_client;
    }

    public Tb_department getDepartment() {
        return department;
    }

    public void setDepartment(Tb_department tb_department) {
        this.department = tb_department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tb_employee tb_employee = (Tb_employee) o;
        if(tb_employee.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tb_employee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tb_employee{" +
            "id=" + id +
            ", designation='" + designation + "'" +
            ", onsite='" + onsite + "'" +
            ", companyname='" + companyname + "'" +
            ", status='" + status + "'" +
            ", supervisor_id='" + supervisor_id + "'" +
            ", created_on='" + created_on + "'" +
            ", created_by='" + created_by + "'" +
            ", modified_on='" + modified_on + "'" +
            ", modified_by='" + modified_by + "'" +
            '}';
    }
}
