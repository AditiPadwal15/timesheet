package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_task;
import portal.repository.Tb_taskRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_task.
 */
@RestController
@RequestMapping("/api")
public class Tb_taskResource {

    private final Logger log = LoggerFactory.getLogger(Tb_taskResource.class);
        
    @Inject
    private Tb_taskRepository tb_taskRepository;
    
    /**
     * POST  /tb_tasks -> Create a new tb_task.
     */
    @RequestMapping(value = "/tb_tasks",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_task> createTb_task(@RequestBody Tb_task tb_task) throws URISyntaxException {
        log.debug("REST request to save Tb_task : {}", tb_task);
        if (tb_task.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_task", "idexists", "A new tb_task cannot already have an ID")).body(null);
        }
        Tb_task result = tb_taskRepository.save(tb_task);
        return ResponseEntity.created(new URI("/api/tb_tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_task", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_tasks -> Updates an existing tb_task.
     */
    @RequestMapping(value = "/tb_tasks",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_task> updateTb_task(@RequestBody Tb_task tb_task) throws URISyntaxException {
        log.debug("REST request to update Tb_task : {}", tb_task);
        if (tb_task.getId() == null) {
            return createTb_task(tb_task);
        }
        Tb_task result = tb_taskRepository.save(tb_task);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_task", tb_task.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_tasks -> get all the tb_tasks.
     */
    @RequestMapping(value = "/tb_tasks",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_task>> getAllTb_tasks(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_tasks");
        Page<Tb_task> page = tb_taskRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_tasks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tb_tasks/:id -> get the "id" tb_task.
     */
    @RequestMapping(value = "/tb_tasks/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_task> getTb_task(@PathVariable Long id) {
        log.debug("REST request to get Tb_task : {}", id);
        Tb_task tb_task = tb_taskRepository.findOne(id);
        return Optional.ofNullable(tb_task)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_tasks/:id -> delete the "id" tb_task.
     */
    @RequestMapping(value = "/tb_tasks/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_task(@PathVariable Long id) {
        log.debug("REST request to delete Tb_task : {}", id);
        tb_taskRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_task", id.toString())).build();
    }
}
