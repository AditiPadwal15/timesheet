package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_department;
import portal.repository.Tb_departmentRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_department.
 */
@RestController
@RequestMapping("/api")
public class Tb_departmentResource {

    private final Logger log = LoggerFactory.getLogger(Tb_departmentResource.class);
        
    @Inject
    private Tb_departmentRepository tb_departmentRepository;
    
    /**
     * POST  /tb_departments -> Create a new tb_department.
     */
    @RequestMapping(value = "/tb_departments",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_department> createTb_department(@RequestBody Tb_department tb_department) throws URISyntaxException {
        log.debug("REST request to save Tb_department : {}", tb_department);
        if (tb_department.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_department", "idexists", "A new tb_department cannot already have an ID")).body(null);
        }
        Tb_department result = tb_departmentRepository.save(tb_department);
        return ResponseEntity.created(new URI("/api/tb_departments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_department", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_departments -> Updates an existing tb_department.
     */
    @RequestMapping(value = "/tb_departments",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_department> updateTb_department(@RequestBody Tb_department tb_department) throws URISyntaxException {
        log.debug("REST request to update Tb_department : {}", tb_department);
        if (tb_department.getId() == null) {
            return createTb_department(tb_department);
        }
        Tb_department result = tb_departmentRepository.save(tb_department);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_department", tb_department.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_departments -> get all the tb_departments.
     */
    @RequestMapping(value = "/tb_departments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_department>> getAllTb_departments(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_departments");
        Page<Tb_department> page = tb_departmentRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_departments");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tb_departments/:id -> get the "id" tb_department.
     */
    @RequestMapping(value = "/tb_departments/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_department> getTb_department(@PathVariable Long id) {
        log.debug("REST request to get Tb_department : {}", id);
        Tb_department tb_department = tb_departmentRepository.findOne(id);
        return Optional.ofNullable(tb_department)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_departments/:id -> delete the "id" tb_department.
     */
    @RequestMapping(value = "/tb_departments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_department(@PathVariable Long id) {
        log.debug("REST request to delete Tb_department : {}", id);
        tb_departmentRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_department", id.toString())).build();
    }
}
