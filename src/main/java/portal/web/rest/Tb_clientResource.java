package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_client;
import portal.repository.Tb_clientRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_client.
 */
@RestController
@RequestMapping("/api")
public class Tb_clientResource {

    private final Logger log = LoggerFactory.getLogger(Tb_clientResource.class);
        
    @Inject
    private Tb_clientRepository tb_clientRepository;
    
    /**
     * POST  /tb_clients -> Create a new tb_client.
     */
    @RequestMapping(value = "/tb_clients",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_client> createTb_client(@RequestBody Tb_client tb_client) throws URISyntaxException {
        log.debug("REST request to save Tb_client : {}", tb_client);
        if (tb_client.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_client", "idexists", "A new tb_client cannot already have an ID")).body(null);
        }
        Tb_client result = tb_clientRepository.save(tb_client);
        return ResponseEntity.created(new URI("/api/tb_clients/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_client", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_clients -> Updates an existing tb_client.
     */
    @RequestMapping(value = "/tb_clients",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_client> updateTb_client(@RequestBody Tb_client tb_client) throws URISyntaxException {
        log.debug("REST request to update Tb_client : {}", tb_client);
        if (tb_client.getId() == null) {
            return createTb_client(tb_client);
        }
        Tb_client result = tb_clientRepository.save(tb_client);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_client", tb_client.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_clients -> get all the tb_clients.
     */
    @RequestMapping(value = "/tb_clients",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_client>> getAllTb_clients(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_clients");
        Page<Tb_client> page = tb_clientRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_clients");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tb_clients/:id -> get the "id" tb_client.
     */
    @RequestMapping(value = "/tb_clients/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_client> getTb_client(@PathVariable Long id) {
        log.debug("REST request to get Tb_client : {}", id);
        Tb_client tb_client = tb_clientRepository.findOne(id);
        return Optional.ofNullable(tb_client)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_clients/:id -> delete the "id" tb_client.
     */
    @RequestMapping(value = "/tb_clients/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_client(@PathVariable Long id) {
        log.debug("REST request to delete Tb_client : {}", id);
        tb_clientRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_client", id.toString())).build();
    }
}
