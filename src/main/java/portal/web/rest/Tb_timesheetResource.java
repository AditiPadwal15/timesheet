package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_timesheet;
import portal.repository.Tb_timesheetRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_timesheet.
 */
@RestController
@RequestMapping("/api")
public class Tb_timesheetResource {

    private final Logger log = LoggerFactory.getLogger(Tb_timesheetResource.class);
        
    @Inject
    private Tb_timesheetRepository tb_timesheetRepository;
    
    /**
     * POST  /tb_timesheets -> Create a new tb_timesheet.
     */
    @RequestMapping(value = "/tb_timesheets",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_timesheet> createTb_timesheet(@RequestBody Tb_timesheet tb_timesheet) throws URISyntaxException {
        log.debug("REST request to save Tb_timesheet : {}", tb_timesheet);
        if (tb_timesheet.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_timesheet", "idexists", "A new tb_timesheet cannot already have an ID")).body(null);
        }
        Tb_timesheet result = tb_timesheetRepository.save(tb_timesheet);
        return ResponseEntity.created(new URI("/api/tb_timesheets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_timesheet", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_timesheets -> Updates an existing tb_timesheet.
     */
    @RequestMapping(value = "/tb_timesheets",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_timesheet> updateTb_timesheet(@RequestBody Tb_timesheet tb_timesheet) throws URISyntaxException {
        log.debug("REST request to update Tb_timesheet : {}", tb_timesheet);
        if (tb_timesheet.getId() == null) {
            return createTb_timesheet(tb_timesheet);
        }
        Tb_timesheet result = tb_timesheetRepository.save(tb_timesheet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_timesheet", tb_timesheet.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_timesheets -> get all the tb_timesheets.
     */
    @RequestMapping(value = "/tb_timesheets",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_timesheet>> getAllTb_timesheets(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_timesheets");
        Page<Tb_timesheet> page = tb_timesheetRepository.findByUserIsCurrentUser(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_timesheets");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
    
    /**
     * GET  /tb_timesheets -> get all the tb_timesheets.
     */
    
  /*  @RequestMapping(value = "/tb_timesheets",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public <List<Tb_timesheet>> getAllTb_timesheets(Pageable pageable)
            throws URISyntaxException {
            log.debug("REST request to get a page of Tb_timesheets");
          
            Page<Tb_timesheet> page=tb_timesheetRepository.findByUserIsCurrentUser();
                 
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_timesheets");
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        }
    */

    /**
     * GET  /tb_timesheets/:id -> get the "id" tb_timesheet.
     */
    @RequestMapping(value = "/tb_timesheets/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_timesheet> getTb_timesheet(@PathVariable Long id) {
        log.debug("REST request to get Tb_timesheet : {}", id);
        Tb_timesheet tb_timesheet = tb_timesheetRepository.findOne(id);
        return Optional.ofNullable(tb_timesheet)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_timesheets/:id -> delete the "id" tb_timesheet.
     */
    @RequestMapping(value = "/tb_timesheets/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_timesheet(@PathVariable Long id) {
        log.debug("REST request to delete Tb_timesheet : {}", id);
        tb_timesheetRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_timesheet", id.toString())).build();
    }
}
