package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_employee;
import portal.repository.Tb_employeeRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_employee.
 */
@RestController
@RequestMapping("/api")
public class Tb_employeeResource {

    private final Logger log = LoggerFactory.getLogger(Tb_employeeResource.class);
        
    @Inject
    private Tb_employeeRepository tb_employeeRepository;
    
    /**
     * POST  /tb_employees -> Create a new tb_employee.
     */
    @RequestMapping(value = "/tb_employees",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_employee> createTb_employee(@RequestBody Tb_employee tb_employee) throws URISyntaxException {
        log.debug("REST request to save Tb_employee : {}", tb_employee);
        if (tb_employee.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_employee", "idexists", "A new tb_employee cannot already have an ID")).body(null);
        }
        Tb_employee result = tb_employeeRepository.save(tb_employee);
        return ResponseEntity.created(new URI("/api/tb_employees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_employee", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_employees -> Updates an existing tb_employee.
     */
    @RequestMapping(value = "/tb_employees",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_employee> updateTb_employee(@RequestBody Tb_employee tb_employee) throws URISyntaxException {
        log.debug("REST request to update Tb_employee : {}", tb_employee);
        if (tb_employee.getId() == null) {
            return createTb_employee(tb_employee);
        }
        Tb_employee result = tb_employeeRepository.save(tb_employee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_employee", tb_employee.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_employees -> get all the tb_employees.
     */
    @RequestMapping(value = "/tb_employees",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_employee>> getAllTb_employees(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_employees");
        Page<Tb_employee> page = tb_employeeRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_employees");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tb_employees/:id -> get the "id" tb_employee.
     */
    @RequestMapping(value = "/tb_employees/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_employee> getTb_employee(@PathVariable Long id) {
        log.debug("REST request to get Tb_employee : {}", id);
        Tb_employee tb_employee = tb_employeeRepository.findOneWithEagerRelationships(id);
        return Optional.ofNullable(tb_employee)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_employees/:id -> delete the "id" tb_employee.
     */
    @RequestMapping(value = "/tb_employees/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_employee(@PathVariable Long id) {
        log.debug("REST request to delete Tb_employee : {}", id);
        tb_employeeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_employee", id.toString())).build();
    }
}
