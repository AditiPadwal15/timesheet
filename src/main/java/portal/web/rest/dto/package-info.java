/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package portal.web.rest.dto;
