package portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import portal.domain.Tb_project;
import portal.repository.Tb_projectRepository;
import portal.web.rest.util.HeaderUtil;
import portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tb_project.
 */
@RestController
@RequestMapping("/api")
public class Tb_projectResource {

    private final Logger log = LoggerFactory.getLogger(Tb_projectResource.class);
        
    @Inject
    private Tb_projectRepository tb_projectRepository;
    
    /**
     * POST  /tb_projects -> Create a new tb_project.
     */
    @RequestMapping(value = "/tb_projects",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_project> createTb_project(@RequestBody Tb_project tb_project) throws URISyntaxException {
        log.debug("REST request to save Tb_project : {}", tb_project);
        if (tb_project.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tb_project", "idexists", "A new tb_project cannot already have an ID")).body(null);
        }
        Tb_project result = tb_projectRepository.save(tb_project);
        return ResponseEntity.created(new URI("/api/tb_projects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("tb_project", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tb_projects -> Updates an existing tb_project.
     */
    @RequestMapping(value = "/tb_projects",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_project> updateTb_project(@RequestBody Tb_project tb_project) throws URISyntaxException {
        log.debug("REST request to update Tb_project : {}", tb_project);
        if (tb_project.getId() == null) {
            return createTb_project(tb_project);
        }
        Tb_project result = tb_projectRepository.save(tb_project);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("tb_project", tb_project.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tb_projects -> get all the tb_projects.
     */
    @RequestMapping(value = "/tb_projects",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Tb_project>> getAllTb_projects(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Tb_projects");
        Page<Tb_project> page = tb_projectRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tb_projects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tb_projects/:id -> get the "id" tb_project.
     */
    @RequestMapping(value = "/tb_projects/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tb_project> getTb_project(@PathVariable Long id) {
        log.debug("REST request to get Tb_project : {}", id);
        Tb_project tb_project = tb_projectRepository.findOne(id);
        return Optional.ofNullable(tb_project)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tb_projects/:id -> delete the "id" tb_project.
     */
    @RequestMapping(value = "/tb_projects/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteTb_project(@PathVariable Long id) {
        log.debug("REST request to delete Tb_project : {}", id);
        tb_projectRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tb_project", id.toString())).build();
    }
}
