package portal.repository;

import portal.domain.Tb_task;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tb_task entity.
 */
public interface Tb_taskRepository extends JpaRepository<Tb_task,Long> {

}
