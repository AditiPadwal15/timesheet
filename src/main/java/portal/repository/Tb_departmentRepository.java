package portal.repository;

import portal.domain.Tb_department;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tb_department entity.
 */
public interface Tb_departmentRepository extends JpaRepository<Tb_department,Long> {

}
