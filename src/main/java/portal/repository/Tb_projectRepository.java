package portal.repository;

import portal.domain.Tb_project;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tb_project entity.
 */
public interface Tb_projectRepository extends JpaRepository<Tb_project,Long> {

}
