package portal.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.User;

import portal.domain.Tb_timesheet;

/**
 * Spring Data JPA repository for the Tb_timesheet entity.
 */
public interface Tb_timesheetRepository extends JpaRepository<Tb_timesheet,Long> {
	
/*	@Query("select distinct tb_timesheet from Tb_timesheet tb_timesheet left join fetch tb_timesheet.employee")
    List<Tb_timesheet> findAllWithEagerRelationships(String userId);
	
	@Query("select tb_timesheet from Tb_timesheet tb_timesheet left join fetch tb_timesheet.employee where tb_timesheet.id =:id")
	Tb_timesheet findOneWithEagerRelationships(@Param("id") Long id);*/

	@Query("select tb_timesheet from Tb_timesheet tb_timesheet where tb_timesheet.employee.user.login = ?#{principal.username}")
	Page<Tb_timesheet> findByUserIsCurrentUser(Pageable pageable);

/*	Tb_timesheet findByUserIsCurrentUser(User currentUser);
*/
}
