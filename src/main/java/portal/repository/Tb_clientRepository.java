package portal.repository;

import portal.domain.Tb_client;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Tb_client entity.
 */
public interface Tb_clientRepository extends JpaRepository<Tb_client,Long> {

}
