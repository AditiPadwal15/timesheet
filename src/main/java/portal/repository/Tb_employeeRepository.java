package portal.repository;

import portal.domain.Tb_employee;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Tb_employee entity.
 */
public interface Tb_employeeRepository extends JpaRepository<Tb_employee,Long> {

    @Query("select distinct tb_employee from Tb_employee tb_employee left join fetch tb_employee.projects")
    List<Tb_employee> findAllWithEagerRelationships();

    @Query("select tb_employee from Tb_employee tb_employee left join fetch tb_employee.projects where tb_employee.id =:id")
    Tb_employee findOneWithEagerRelationships(@Param("id") Long id);

}
