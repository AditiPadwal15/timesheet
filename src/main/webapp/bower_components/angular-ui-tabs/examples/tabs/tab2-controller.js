'use strict';

angular.module('angular-tabs.demo').controller('tab2Ctrl', function ($scope) {
    $scope.model = $scope.model || 'Model provided from tab2Ctrl';
});
