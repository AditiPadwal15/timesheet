'use strict';

angular.module('timesheetApp')
    .factory('Tb_timesheet', function ($resource, DateUtils) {
        return $resource('api/tb_timesheets/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.dateoftimesheet = DateUtils.convertLocaleDateFromServer(data.dateoftimesheet);
                    data.created_on = DateUtils.convertLocaleDateFromServer(data.created_on);
                    data.approvaldate = DateUtils.convertLocaleDateFromServer(data.approvaldate);
                    data.modified_on = DateUtils.convertLocaleDateFromServer(data.modified_on);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.dateoftimesheet = DateUtils.convertLocaleDateToServer(data.dateoftimesheet);
                    data.created_on = DateUtils.convertLocaleDateToServer(data.created_on);
                    data.approvaldate = DateUtils.convertLocaleDateToServer(data.approvaldate);
                    data.modified_on = DateUtils.convertLocaleDateToServer(data.modified_on);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.dateoftimesheet = DateUtils.convertLocaleDateToServer(data.dateoftimesheet);
                    data.created_on = DateUtils.convertLocaleDateToServer(data.created_on);
                    data.approvaldate = DateUtils.convertLocaleDateToServer(data.approvaldate);
                    data.modified_on = DateUtils.convertLocaleDateToServer(data.modified_on);
                    return angular.toJson(data);
                }
            }
        });
    });
