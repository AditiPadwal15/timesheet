'use strict';

angular.module('timesheetApp')
    .factory('Tb_project', function ($resource, DateUtils) {
        return $resource('api/tb_projects/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.start_date = DateUtils.convertLocaleDateFromServer(data.start_date);
                    data.end_date = DateUtils.convertLocaleDateFromServer(data.end_date);
                    data.created_on = DateUtils.convertDateTimeFromServer(data.created_on);
                    data.modified_on = DateUtils.convertDateTimeFromServer(data.modified_on);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.start_date = DateUtils.convertLocaleDateToServer(data.start_date);
                    data.end_date = DateUtils.convertLocaleDateToServer(data.end_date);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.start_date = DateUtils.convertLocaleDateToServer(data.start_date);
                    data.end_date = DateUtils.convertLocaleDateToServer(data.end_date);
                    return angular.toJson(data);
                }
            }
        });
    });
