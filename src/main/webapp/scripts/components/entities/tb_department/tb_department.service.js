'use strict';

angular.module('timesheetApp')
    .factory('Tb_department', function ($resource, DateUtils) {
        return $resource('api/tb_departments/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.created_on = DateUtils.convertDateTimeFromServer(data.created_on);
                    data.modified_on = DateUtils.convertDateTimeFromServer(data.modified_on);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
