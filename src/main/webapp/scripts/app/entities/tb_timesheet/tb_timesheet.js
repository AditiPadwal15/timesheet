'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_timesheet', {
                parent: 'entity',
                url: '/tb_timesheets',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_timesheet.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_timesheet/tb_timesheets.html',
                        controller: 'Tb_timesheetController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_timesheet');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_timesheet.detail', {
                parent: 'entity',
                url: '/tb_timesheet/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_timesheet.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_timesheet/tb_timesheet-detail.html',
                        controller: 'Tb_timesheetDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_timesheet');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_timesheet', function($stateParams, Tb_timesheet) {
                        return Tb_timesheet.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_timesheet.new', {
                parent: 'tb_timesheet',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_timesheet/tb_timesheet-dialog.html',
                        controller: 'Tb_timesheetDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    dateoftimesheet: null,
                                    workinghours: null,
                                    billablehours: null,
                                    shift: null,
                                    created_on: null,
                                    created_by: null,
                                    approvaldate: null,
                                    status: null,
                                    remark: null,
                                    weekno: null,
                                    modified_on: null,
                                    modified_by: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_timesheet', null, { reload: true });
                    }, function() {
                        $state.go('tb_timesheet');
                    })
                }]
            })
            .state('tb_timesheet.edit', {
                parent: 'tb_timesheet',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_timesheet/tb_timesheet-dialog.html',
                        controller: 'Tb_timesheetDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_timesheet', function(Tb_timesheet) {
                                return Tb_timesheet.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_timesheet', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_timesheet.delete', {
                parent: 'tb_timesheet',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_timesheet/tb_timesheet-delete-dialog.html',
                        controller: 'Tb_timesheetDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_timesheet', function(Tb_timesheet) {
                                return Tb_timesheet.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_timesheet', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
