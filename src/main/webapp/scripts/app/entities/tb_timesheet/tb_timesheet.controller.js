'use strict';

angular.module('timesheetApp')
    .controller('Tb_timesheetController', function ($scope, $state, Tb_timesheet, ParseLinks) {

        $scope.tb_timesheets = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            Tb_timesheet.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.tb_timesheets = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tb_timesheet = {
                name: null,
                dateoftimesheet: null,
                workinghours: null,
                billablehours: null,
                shift: null,
                created_on: null,
                created_by: null,
                approvaldate: null,
                status: null,
                remark: null,
                weekno: null,
                modified_on: null,
                modified_by: null,
                id: null
            };
        };
    });
