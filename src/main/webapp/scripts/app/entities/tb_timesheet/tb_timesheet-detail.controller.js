'use strict';

angular.module('timesheetApp')
    .controller('Tb_timesheetDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_timesheet, Tb_employee) {
        $scope.tb_timesheet = entity;
        $scope.load = function (id) {
            Tb_timesheet.get({id: id}, function(result) {
                $scope.tb_timesheet = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_timesheetUpdate', function(event, result) {
            $scope.tb_timesheet = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
