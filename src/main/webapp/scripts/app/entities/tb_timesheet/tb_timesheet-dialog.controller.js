'use strict';

angular.module('timesheetApp').controller('Tb_timesheetDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tb_timesheet', 'Tb_employee',
        function($scope, $stateParams, $uibModalInstance, entity, Tb_timesheet, Tb_employee) {

        $scope.tb_timesheet = entity;
        $scope.tb_employees = Tb_employee.query();
        $scope.load = function(id) {
            Tb_timesheet.get({id : id}, function(result) {
                $scope.tb_timesheet = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_timesheetUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_timesheet.id != null) {
                Tb_timesheet.update($scope.tb_timesheet, onSaveSuccess, onSaveError);
            } else {
                Tb_timesheet.save($scope.tb_timesheet, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForDateoftimesheet = {};

        $scope.datePickerForDateoftimesheet.status = {
            opened: false
        };

        $scope.datePickerForDateoftimesheetOpen = function($event) {
            $scope.datePickerForDateoftimesheet.status.opened = true;
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForApprovaldate = {};

        $scope.datePickerForApprovaldate.status = {
            opened: false
        };

        $scope.datePickerForApprovaldateOpen = function($event) {
            $scope.datePickerForApprovaldate.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
