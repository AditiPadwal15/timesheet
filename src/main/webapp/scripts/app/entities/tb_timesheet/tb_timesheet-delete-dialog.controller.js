'use strict';

angular.module('timesheetApp')
	.controller('Tb_timesheetDeleteController', function($scope, $uibModalInstance, entity, Tb_timesheet) {

        $scope.tb_timesheet = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_timesheet.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
