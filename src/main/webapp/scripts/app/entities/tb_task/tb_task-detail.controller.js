'use strict';

angular.module('timesheetApp')
    .controller('Tb_taskDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_task, Tb_employee, Tb_project) {
        $scope.tb_task = entity;
        $scope.load = function (id) {
            Tb_task.get({id: id}, function(result) {
                $scope.tb_task = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_taskUpdate', function(event, result) {
            $scope.tb_task = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
