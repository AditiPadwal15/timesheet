'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_task', {
                parent: 'entity',
                url: '/tb_tasks',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_task.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_task/tb_tasks.html',
                        controller: 'Tb_taskController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_task');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_task.detail', {
                parent: 'entity',
                url: '/tb_task/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_task.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_task/tb_task-detail.html',
                        controller: 'Tb_taskDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_task');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_task', function($stateParams, Tb_task) {
                        return Tb_task.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_task.new', {
                parent: 'tb_task',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_task/tb_task-dialog.html',
                        controller: 'Tb_taskDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    hours: null,
                                    created_on: null,
                                    created_by: null,
                                    modified_on: null,
                                    modified_by: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_task', null, { reload: true });
                    }, function() {
                        $state.go('tb_task');
                    })
                }]
            })
            .state('tb_task.edit', {
                parent: 'tb_task',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_task/tb_task-dialog.html',
                        controller: 'Tb_taskDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_task', function(Tb_task) {
                                return Tb_task.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_task', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_task.delete', {
                parent: 'tb_task',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_task/tb_task-delete-dialog.html',
                        controller: 'Tb_taskDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_task', function(Tb_task) {
                                return Tb_task.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_task', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
