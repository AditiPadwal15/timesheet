'use strict';

angular.module('timesheetApp')
	.controller('Tb_taskDeleteController', function($scope, $uibModalInstance, entity, Tb_task) {

        $scope.tb_task = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_task.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
