'use strict';

angular.module('timesheetApp').controller('Tb_taskDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tb_task', 'Tb_employee', 'Tb_project',
        function($scope, $stateParams, $uibModalInstance, entity, Tb_task, Tb_employee, Tb_project) {

        $scope.tb_task = entity;
        $scope.tb_employees = Tb_employee.query();
        $scope.tb_projects = Tb_project.query();
        $scope.load = function(id) {
            Tb_task.get({id : id}, function(result) {
                $scope.tb_task = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_taskUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_task.id != null) {
                Tb_task.update($scope.tb_task, onSaveSuccess, onSaveError);
            } else {
                Tb_task.save($scope.tb_task, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
