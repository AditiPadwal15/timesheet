'use strict';

angular.module('timesheetApp')
    .controller('Tb_projectController', function ($scope, $state, Tb_project, ParseLinks) {

        $scope.tb_projects = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            Tb_project.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.tb_projects = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tb_project = {
                name: null,
                description: null,
                start_date: null,
                end_date: null,
                created_on: null,
                created_by: null,
                modified_on: null,
                modified_by: null,
                status: null,
                id: null
            };
        };
    });
