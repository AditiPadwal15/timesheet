'use strict';

angular.module('timesheetApp')
	.controller('Tb_projectDeleteController', function($scope, $uibModalInstance, entity, Tb_project) {

        $scope.tb_project = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_project.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
