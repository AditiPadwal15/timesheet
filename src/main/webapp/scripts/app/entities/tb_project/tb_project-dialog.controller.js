'use strict';

angular.module('timesheetApp').controller('Tb_projectDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tb_project', 'Tb_employee', 'Tb_client',
        function($scope, $stateParams, $uibModalInstance, entity, Tb_project, Tb_employee, Tb_client) {

        $scope.tb_project = entity;
        $scope.tb_employees = Tb_employee.query();
        $scope.tb_clients = Tb_client.query();
        $scope.load = function(id) {
            Tb_project.get({id : id}, function(result) {
                $scope.tb_project = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_projectUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_project.id != null) {
                Tb_project.update($scope.tb_project, onSaveSuccess, onSaveError);
            } else {
                Tb_project.save($scope.tb_project, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForStart_date = {};

        $scope.datePickerForStart_date.status = {
            opened: false
        };

        $scope.datePickerForStart_dateOpen = function($event) {
            $scope.datePickerForStart_date.status.opened = true;
        };
        $scope.datePickerForEnd_date = {};

        $scope.datePickerForEnd_date.status = {
            opened: false
        };

        $scope.datePickerForEnd_dateOpen = function($event) {
            $scope.datePickerForEnd_date.status.opened = true;
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
