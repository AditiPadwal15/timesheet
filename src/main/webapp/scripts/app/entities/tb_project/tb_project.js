'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_project', {
                parent: 'entity',
                url: '/tb_projects',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_project.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_project/tb_projects.html',
                        controller: 'Tb_projectController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_project');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_project.detail', {
                parent: 'entity',
                url: '/tb_project/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_project.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_project/tb_project-detail.html',
                        controller: 'Tb_projectDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_project');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_project', function($stateParams, Tb_project) {
                        return Tb_project.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_project.new', {
                parent: 'tb_project',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_project/tb_project-dialog.html',
                        controller: 'Tb_projectDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    start_date: null,
                                    end_date: null,
                                    created_on: null,
                                    created_by: null,
                                    modified_on: null,
                                    modified_by: null,
                                    status: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_project', null, { reload: true });
                    }, function() {
                        $state.go('tb_project');
                    })
                }]
            })
            .state('tb_project.edit', {
                parent: 'tb_project',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_project/tb_project-dialog.html',
                        controller: 'Tb_projectDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_project', function(Tb_project) {
                                return Tb_project.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_project', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_project.delete', {
                parent: 'tb_project',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_project/tb_project-delete-dialog.html',
                        controller: 'Tb_projectDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_project', function(Tb_project) {
                                return Tb_project.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_project', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
