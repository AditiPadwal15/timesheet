'use strict';

angular.module('timesheetApp')
    .controller('Tb_projectDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_project, Tb_employee, Tb_client) {
        $scope.tb_project = entity;
        $scope.load = function (id) {
            Tb_project.get({id: id}, function(result) {
                $scope.tb_project = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_projectUpdate', function(event, result) {
            $scope.tb_project = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
