'use strict';

angular.module('timesheetApp')
    .controller('Tb_clientDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_client) {
        $scope.tb_client = entity;
        $scope.load = function (id) {
            Tb_client.get({id: id}, function(result) {
                $scope.tb_client = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_clientUpdate', function(event, result) {
            $scope.tb_client = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
