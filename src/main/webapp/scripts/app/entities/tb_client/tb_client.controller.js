'use strict';

angular.module('timesheetApp')
    .controller('Tb_clientController', function ($scope, $state, Tb_client, ParseLinks) {

        $scope.tb_clients = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            Tb_client.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.tb_clients = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tb_client = {
                name: null,
                description: null,
                created_on: null,
                created_by: null,
                modified_on: null,
                modified_by: null,
                status: null,
                id: null
            };
        };
    });
