'use strict';

angular.module('timesheetApp').controller('Tb_clientDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tb_client',
        function($scope, $stateParams, $uibModalInstance, entity, Tb_client) {

        $scope.tb_client = entity;
        $scope.load = function(id) {
            Tb_client.get({id : id}, function(result) {
                $scope.tb_client = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_clientUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_client.id != null) {
                Tb_client.update($scope.tb_client, onSaveSuccess, onSaveError);
            } else {
                Tb_client.save($scope.tb_client, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
