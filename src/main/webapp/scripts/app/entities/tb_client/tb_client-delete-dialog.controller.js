'use strict';

angular.module('timesheetApp')
	.controller('Tb_clientDeleteController', function($scope, $uibModalInstance, entity, Tb_client) {

        $scope.tb_client = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_client.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
