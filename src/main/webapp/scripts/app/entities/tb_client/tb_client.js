'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_client', {
                parent: 'entity',
                url: '/tb_clients',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_client.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_client/tb_clients.html',
                        controller: 'Tb_clientController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_client');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_client.detail', {
                parent: 'entity',
                url: '/tb_client/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_client.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_client/tb_client-detail.html',
                        controller: 'Tb_clientDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_client');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_client', function($stateParams, Tb_client) {
                        return Tb_client.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_client.new', {
                parent: 'tb_client',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_client/tb_client-dialog.html',
                        controller: 'Tb_clientDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    created_on: null,
                                    created_by: null,
                                    modified_on: null,
                                    modified_by: null,
                                    status: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_client', null, { reload: true });
                    }, function() {
                        $state.go('tb_client');
                    })
                }]
            })
            .state('tb_client.edit', {
                parent: 'tb_client',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_client/tb_client-dialog.html',
                        controller: 'Tb_clientDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_client', function(Tb_client) {
                                return Tb_client.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_client', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_client.delete', {
                parent: 'tb_client',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_client/tb_client-delete-dialog.html',
                        controller: 'Tb_clientDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_client', function(Tb_client) {
                                return Tb_client.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_client', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
