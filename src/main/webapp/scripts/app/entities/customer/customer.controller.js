'use strict';

angular.module('timesheetApp')
    .controller('CustomerController', function ($scope, $state, Customer) {

        $scope.customers = [];
        $scope.loadAll = function() {
            Customer.query(function(result) {
               $scope.customers = result;
            });
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.customer = {
                customer_Name: null,
                description: null,
                id: null
            };
        };
    });
