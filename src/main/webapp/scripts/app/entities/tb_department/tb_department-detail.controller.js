'use strict';

angular.module('timesheetApp')
    .controller('Tb_departmentDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_department) {
        $scope.tb_department = entity;
        $scope.load = function (id) {
            Tb_department.get({id: id}, function(result) {
                $scope.tb_department = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_departmentUpdate', function(event, result) {
            $scope.tb_department = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
