'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_department', {
                parent: 'entity',
                url: '/tb_departments',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_department.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_department/tb_departments.html',
                        controller: 'Tb_departmentController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_department');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_department.detail', {
                parent: 'entity',
                url: '/tb_department/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_department.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_department/tb_department-detail.html',
                        controller: 'Tb_departmentDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_department');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_department', function($stateParams, Tb_department) {
                        return Tb_department.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_department.new', {
                parent: 'tb_department',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_department/tb_department-dialog.html',
                        controller: 'Tb_departmentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    created_on: null,
                                    created_by: null,
                                    modified_on: null,
                                    modified_by: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_department', null, { reload: true });
                    }, function() {
                        $state.go('tb_department');
                    })
                }]
            })
            .state('tb_department.edit', {
                parent: 'tb_department',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_department/tb_department-dialog.html',
                        controller: 'Tb_departmentDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_department', function(Tb_department) {
                                return Tb_department.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_department', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_department.delete', {
                parent: 'tb_department',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_department/tb_department-delete-dialog.html',
                        controller: 'Tb_departmentDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_department', function(Tb_department) {
                                return Tb_department.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_department', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
