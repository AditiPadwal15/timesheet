'use strict';

angular.module('timesheetApp')
	.controller('Tb_departmentDeleteController', function($scope, $uibModalInstance, entity, Tb_department) {

        $scope.tb_department = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_department.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
