'use strict';

angular.module('timesheetApp').controller('Tb_departmentDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Tb_department',
        function($scope, $stateParams, $uibModalInstance, entity, Tb_department) {

        $scope.tb_department = entity;
        $scope.load = function(id) {
            Tb_department.get({id : id}, function(result) {
                $scope.tb_department = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_departmentUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_department.id != null) {
                Tb_department.update($scope.tb_department, onSaveSuccess, onSaveError);
            } else {
                Tb_department.save($scope.tb_department, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
