'use strict';

angular.module('timesheetApp')
    .controller('Tb_employeeDetailController', function ($scope, $rootScope, $stateParams, entity, Tb_employee, User, Tb_project, Tb_task, Tb_client, Tb_department) {
        $scope.tb_employee = entity;
        $scope.load = function (id) {
            Tb_employee.get({id: id}, function(result) {
                $scope.tb_employee = result;
            });
        };
        var unsubscribe = $rootScope.$on('timesheetApp:tb_employeeUpdate', function(event, result) {
            $scope.tb_employee = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
