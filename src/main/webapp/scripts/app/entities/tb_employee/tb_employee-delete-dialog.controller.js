'use strict';

angular.module('timesheetApp')
	.controller('Tb_employeeDeleteController', function($scope, $uibModalInstance, entity, Tb_employee) {

        $scope.tb_employee = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tb_employee.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
