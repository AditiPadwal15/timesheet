'use strict';

angular.module('timesheetApp')
    .controller('Tb_employeeController', function ($scope, $state, Tb_employee, ParseLinks) {

        $scope.tb_employees = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            Tb_employee.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.tb_employees = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tb_employee = {
                designation: null,
                onsite: null,
                companyname: null,
                status: null,
                supervisor_id: null,
                created_on: null,
                created_by: null,
                modified_on: null,
                modified_by: null,
                id: null
            };
        };
    });
