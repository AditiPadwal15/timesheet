'use strict';

angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tb_employee', {
                parent: 'entity',
                url: '/tb_employees',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_employee.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_employee/tb_employees.html',
                        controller: 'Tb_employeeController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_employee');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('tb_employee.detail', {
                parent: 'entity',
                url: '/tb_employee/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'timesheetApp.tb_employee.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tb_employee/tb_employee-detail.html',
                        controller: 'Tb_employeeDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('tb_employee');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Tb_employee', function($stateParams, Tb_employee) {
                        return Tb_employee.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tb_employee.new', {
                parent: 'tb_employee',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_employee/tb_employee-dialog.html',
                        controller: 'Tb_employeeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    designation: null,
                                    onsite: null,
                                    companyname: null,
                                    status: null,
                                    supervisor_id: null,
                                    created_on: null,
                                    created_by: null,
                                    modified_on: null,
                                    modified_by: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tb_employee', null, { reload: true });
                    }, function() {
                        $state.go('tb_employee');
                    })
                }]
            })
            .state('tb_employee.edit', {
                parent: 'tb_employee',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_employee/tb_employee-dialog.html',
                        controller: 'Tb_employeeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tb_employee', function(Tb_employee) {
                                return Tb_employee.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_employee', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tb_employee.delete', {
                parent: 'tb_employee',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tb_employee/tb_employee-delete-dialog.html',
                        controller: 'Tb_employeeDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tb_employee', function(Tb_employee) {
                                return Tb_employee.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tb_employee', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
