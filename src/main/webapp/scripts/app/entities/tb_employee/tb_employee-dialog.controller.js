'use strict';

angular.module('timesheetApp').controller('Tb_employeeDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Tb_employee', 'User', 'Tb_project', 'Tb_task', 'Tb_client', 'Tb_department',
        function($scope, $stateParams, $uibModalInstance, $q, entity, Tb_employee, User, Tb_project, Tb_task, Tb_client, Tb_department) {

        $scope.tb_employee = entity;
        $scope.users = User.query();
        $scope.tb_projects = Tb_project.query();
        $scope.tb_tasks = Tb_task.query();
        $scope.tb_clients = Tb_client.query();
        $scope.tb_departments = Tb_department.query();
        $scope.load = function(id) {
            Tb_employee.get({id : id}, function(result) {
                $scope.tb_employee = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('timesheetApp:tb_employeeUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tb_employee.id != null) {
                Tb_employee.update($scope.tb_employee, onSaveSuccess, onSaveError);
            } else {
                Tb_employee.save($scope.tb_employee, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreated_on = {};

        $scope.datePickerForCreated_on.status = {
            opened: false
        };

        $scope.datePickerForCreated_onOpen = function($event) {
            $scope.datePickerForCreated_on.status.opened = true;
        };
        $scope.datePickerForModified_on = {};

        $scope.datePickerForModified_on.status = {
            opened: false
        };

        $scope.datePickerForModified_onOpen = function($event) {
            $scope.datePickerForModified_on.status.opened = true;
        };
}]);
