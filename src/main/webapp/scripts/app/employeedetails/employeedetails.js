angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('employeedetails', {
                parent: 'site',
                url: '/employeedetails',
                data: {
                    authorities: ['ROLE_USER'], 
                    pageTitle: 'employeedetails.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/employeedetails/employeedetails.html',
                        controller: 'EmployeedetailsController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('employeedetails');
                        return $translate.refresh();
                    }]
                }
            });
    });