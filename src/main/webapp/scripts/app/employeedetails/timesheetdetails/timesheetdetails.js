angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            /*.state('timesheet', {
                parent: 'employee',
                url: '/timesheet',
                data: {
                    authorities: ['ROLE_USER'], 
                    pageTitle: 'timesheet.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/employee/timesheet/timesheet.html',
                        controller: 'TimesheetController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('timesheet');
                        return $translate.refresh();
                    }]
                }
            })
        */
        .state('timesheetdetails', {
            parent: 'employeedetails',
            url: '/timesheetdetails',
            data: {
                authorities: ['ROLE_USER'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/employeedetails/timesheetdetails/timesheetdetails-dialog.html',
                    controller: 'TimesheetDialogDetailsController',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                customer_Name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function(result) {
                    $state.go('employeedetails', null, { reload: true });
                }, function() {
                    $state.go('employeedetails');
                })
            }]
        }) 
        
        
        
        
        
        
        
    });

