'use strict';

angular.module('timesheetApp').controller('TimesheetDialogDetailsController',
    ['$scope', '$stateParams', '$uibModalInstance', 
        function($scope, $stateParams, $uibModalInstance) {

      
       
       /* var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.customer.id != null) {
                Customer.update($scope.customer, onSaveSuccess, onSaveError);
            } else {
                Customer.save($scope.customer, onSaveSuccess, onSaveError);
            }
        };
*/
       
    	$scope.invoice = {
    	        items: [{
    	            srno: 1,
    	            client: 'Select',
    	            project: 'Select',
    	            hours: 0}]
    	    };

    	    $scope.addItem = function() {
    	    	
    	        $scope.invoice.items.push({
    	        	srno:'',
    	            client: '',
    	            project: '',
    	            hours: ''
    	        });
    	    },
    	    
    	    $scope.removeItem = function(index) {
    	        $scope.invoice.items.splice(index, 1);
    	    },

    	    
    	    $scope.total = function() {
    	        var total = 0;
    	        angular.forEach($scope.invoice.items, function(item) {
    	            total += item.hours ;
    	        })
    	        return total;
    	    }
   	   
    	    $scope.serno = function() {
    	        var serno = 0;
    	        angular.forEach($scope.invoice.items, function(item) {
    	            serno += item.srno ;
    	           
    	        })
    	        serno++;
    	        return serno;
    	    }
        
            $scope.clear = function() {
                $uibModalInstance.dismiss('cancel');
            };
                
        
        
        
}]);




	