angular.module('timesheetApp')
  .controller('EmployeedetailsController', function($scope, $state) {
    /* config object */
	  $scope.redirectToTimesheet= function () {
		  
		  $state.go('timesheetdetails');
		  
		  	};
		  	
  /* -- -------------------------------------------
   * -- Angular to display week no
	 -- ------------------------------------------*/
		  	Date.prototype.getWeekNo = function() {
		  	    var onejan = new Date(this.getFullYear(),0,1);
		  	    var today = new Date(this.getFullYear(),this.getMonth(),this.getDate());
		  	    var dayOfYear = ((today - onejan +1)/86400000);
		  	    return Math.ceil(dayOfYear/7)
		  	};


		  	jQuery(function(){  
		  	    var today = new Date();
		  	    var weekno = today.getWeekNo();
		  	    $("#weekNo").html(weekno);
		  	})  	
		  	
	 /* -- -------------------------------------------
   * -- Angular to display date and day in the date field in week
	 -- ------------------------------------------*/	  	
		  	
		  	var currentDate = moment();
		    var weekStart = currentDate.clone().startOf('week');
		    var weekEnd = currentDate.clone().endOf('week');

		    var days = [];
		    for (i = 0; i <= 6; i++) {

		        days.push(moment(weekStart).add(i, 'days').format("MMMM Do,dddd"));


		    };
		    $scope.weekDays = days;
		    
	/* -- -------------------------------------------------------------------------
	 * -- Disable timesheet and related buttons if holiday or leave label clicked
	   -- ------------------------------------------------------------------------*/
		    
		    $scope.setLeave = function () {
		    	
		    	angular.element(document.getElementById('timesheetbtn'))[0].disabled = true;
		    	angular.element(document.getElementById('dtlsbtn'))[0].disabled = true;
		    	angular.element(document.getElementById('editbtn'))[0].disabled = true;
	           
	        };
		    
	 /* -- -------------------------------------------------------------------------
	  * -- Enable timesheet and related buttons if working label clicked
	    -- ------------------------------------------------------------------------*/
	    		    
	    		    $scope.setWorking = function () {
	    		    	
	    		    	angular.element(document.getElementById('timesheetbtn'))[0].disabled = false;
	    		    	angular.element(document.getElementById('dtlsbtn'))[0].disabled = false;
	    		    	angular.element(document.getElementById('editbtn'))[0].disabled = false;
	    	           
	    	        };
	    	        
	    	        
	/* -- ------------------------------------------------------------------------
	 * -- Change Row Color if working/holiday/
	 */    		    	  	
		  	
	    	        $scope.idSelectedVote = null;
	    	        $scope.setSelected = function(idSelectedVote) {
	    	           $scope.idSelectedVote = idSelectedVote;
	    	           console.log(idSelectedVote);
	    	        }
});