angular
		.module('timesheetApp')
		.config(
				function($stateProvider) {
					$stateProvider
							.state(
									'managerdetails',
									{
										parent : 'site',
										url : '/managerdetails',
										data : {
											authorities : [ 'ROLE_MANAGER' ],
											pageTitle : 'managerdetails.title'
										},
										views : {
											'content@' : {
												templateUrl : 'scripts/app/managerdetails/managerdetails.html',
												controller : 'ManagerdetailsController'
											}
										},
										resolve : {
											translatePartialLoader : [
													'$translate',
													'$translatePartialLoader',
													function($translate,
															$translatePartialLoader) {
														$translatePartialLoader
																.addPart('managerdetails');
														return $translate
																.refresh();
													} ]
										}
									})

							.state(
									'managerapproval',
									{
										parent : 'site',
										url : '/managerapproval',
										data : {
											authorities : [ 'ROLE_MANAGER' ],
											pageTitle : 'managerapproval.title'
										},
										views : {
											'content@' : {
												templateUrl : 'scripts/app/managerdetails/managerapproval/managerapproval.html',
												controller : 'ManagerApprovalController'
											}
										},
										resolve : {
											translatePartialLoader : [
													'$translate',
													'$translatePartialLoader',
													function($translate,
															$translatePartialLoader) {
														$translatePartialLoader
																.addPart('managerapproval');
														return $translate
																.refresh();
													} ]
										}
									})

				});