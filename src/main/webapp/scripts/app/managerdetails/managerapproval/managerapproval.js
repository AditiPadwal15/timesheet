angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            /*.state('timesheet', {
                parent: 'employee',
                url: '/timesheet',
                data: {
                    authorities: ['ROLE_USER'], 
                    pageTitle: 'timesheet.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/employee/timesheet/timesheet.html',
                        controller: 'TimesheetController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('timesheet');
                        return $translate.refresh();
                    }]
                }
            })
        */
    /*    .state('managerapproval', {
            parent: 'managerdetails',
            url: '/managerapproval',
            data: {
                authorities: ['ROLE_USER'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/managerdetails/managerapproval/managerapproval-dialog.html',
                    controller: 'ManagerDialogDetailsController',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                customer_Name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function(result) {
                    $state.go('managerdetails', null, { reload: true });
                }, function() {
                    $state.go('managerdetails');
                })
            }]
        }) 
        
        */
        
        
        
         .state('managerapproval1', {
                parent: 'managerdetails',
                url: '/managerapproval',
                data: {
                    authorities: ['ROLE_MANAGER'], 
                    pageTitle: 'managerapproval.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/managerdetails/managerapproval/managerapproval.html',
												                        controller : 'ManagerApprovalController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('managerapproval');
                        return $translate.refresh();
                    }]
                }
            })
        
        
        
    });

