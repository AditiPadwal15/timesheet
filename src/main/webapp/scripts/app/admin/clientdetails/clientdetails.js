angular.module('timesheetApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('clientdetails', {
                parent: 'admin',
                url: '/clientdetails',
                data: {
                    authorities: ['ROLE_ADMIN'], 
                    pageTitle: 'clientdetails.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/admin/clientdetails/clientdetails.html',
                        controller: 'ClientdetailsController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('clientdetails');
                        return $translate.refresh();
                    }]
                }
            })
        
        .state('addprojectdetails', {
            parent: 'clientdetails',
            url: '/addprojectdetails',
            data: {
                authorities: ['ROLE_ADMIN'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/admin/clientdetails/addprojectdetails-dialog.html',
                    controller: 'AddProjectDetailsDialogController',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                customer_Name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
               }).result.then(function(result) {
                   $state.go('clientdetails', null, { reload: true });
               }, function() {
                   $state.go('clientdetails');
               })
                         
               
            }]
        }) 
        
         .state('addclientdetails', {
            parent: 'clientdetails',
            url: '/addclientdetails',
            data: {
                authorities: ['ROLE_ADMIN'],
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/admin/clientdetails/addclientdetails-dialog.html',
                    controller: 'AddClientDialogDetailsController',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                customer_Name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function(result) {
                    $state.go('clientdetails', null, { reload: true });
                }, function() {
                    $state.go('clientdetails');
                })
            }]
        }) 
        
        
        
          $stateProvider
            .state('projectlistdetails', {
                parent: 'admin',
                url: '/projectlistdetails',
                data: {
                    authorities: ['ROLE_USER'], 
                    pageTitle: 'projectlistdetails.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/admin/listofprojectdetails/projectlistdetails.html',
                        
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('projectlistdetails');
                        return $translate.refresh();
                    }]
                }
            })
        
        
    });